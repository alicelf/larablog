<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define( App\User::class, function ( Faker\Generator $faker ) {
	static $password;

	return [
		'name'           => $faker->name,
		'email'          => $faker->unique()->safeEmail,
		'password'       => $password ?: $password = bcrypt( 'secret' ),
		'remember_token' => str_random( 10 ),
	];
} );

// php artisan tinker
// factory(\App\Post::class, 20)->make()
$factory->define( \App\Post::class, function ( Faker\Generator $faker ) {
	return [
		'user_id' => function () {
			return factory(\App\User::class)->create()->id;
		},
		'title'   => $faker->sentence,
		'body'    => $faker->paragraph
	];
} );
