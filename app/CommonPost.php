<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CommonPost extends Eloquent
{

	protected $fillable = [ 'title', 'body' ];

}
