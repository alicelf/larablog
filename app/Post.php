<?php

namespace App;

use Carbon\Carbon;

class Post extends CommonPost
{

	protected $fillable = [ 'user_id', 'title', 'body' ];

	public function comments()
	{
		return $this->hasMany( Comment::class );
	}

	public function user() // $comment->post->user
	{
		return $this->belongsTo( User::class );
	}

	public function author() // $comment->post->author
	{
		return $this->belongsTo( User::class, 'user_id' );
	}

	public function addComment( $body )
	{
		$this->comments()->create( compact( 'body' ) );
	}

	// Archive filter
	public function scopeArchiveFilter( $query, $filters )
	{
		if ( $month = $filters[ 'month' ] ) {
			$query->whereMonth( 'created_at', Carbon::parse( $month )->month );
		}

		if ( $year = $filters[ 'year' ] ) {
			$query->whereYear( 'created_at', $year );
		}
	}

	public static function archives()
	{
		return static::selectRaw( 'year(created_at) year, monthname(created_at) month, count(*) published' )
		             ->groupBy( 'year', 'month' )
		             ->orderByRaw( 'min(created_at) desc' )
		             ->get()->toArray();
	}

}
