<?php

namespace App\Http\Controllers;

use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PostController extends Controller
{

	public function __construct()
	{
		$this->middleware( 'auth' )->except( [ 'index', 'show' ] );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$posts = Post::latest()
		             ->archiveFilter( request( [ 'month', 'year' ] ) )
		             ->get();

		$archives = Post::archives();

		return view( 'front.posts.index', compact( 'posts' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view( 'front.posts.create' );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request )
	{
		$this->validate( $request, [
			'title' => 'required|unique:posts|max:255',
			'body'  => 'required'
		] );

		auth()->user()->publish(
			new Post( request( [ 'title', 'body' ] ) )
		);

//		Post::create( [
//			'title'   => $request->input( 'title' ),
//			'body'    => $request->input( 'body' ),
//			'user_id' => auth()->id(),
//		] );

		return back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Post $post
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( Post $post )
	{

		return view( 'front.posts.show', compact( 'post' ) );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Post $post
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( Post $post )
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \App\Post $post
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, Post $post )
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Post $post
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( Post $post )
	{
		//
	}
}
