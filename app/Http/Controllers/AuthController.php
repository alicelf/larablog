<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{

	public function __construct()
	{
		$this->middleware( 'guest' )->except( [ 'logout' ] );
	}

	public function register()
	{
		return view( 'auth.register' );
	}

	public function login()
	{
		return view( 'auth.login' );
	}

	public function create()
	{
		$this->validate( request(), [
			'name'     => 'required|min:3',
			'email'    => 'required|email|unique:users',
			'password' => 'required|confirmed',
		] );

		$user = User::create( [
			'name'     => request( 'name' ),
			'email'    => request( 'email' ),
			'password' => bcrypt( request( 'password' ) )
		] );

		auth()->login( $user );

		return redirect()->home();

	}

	public function signin()
	{
		$this->validate( request(), [
			'email'    => 'required',
			'password' => 'required'
		] );

		if ( ! auth()->attempt( request( [ 'email', 'password' ] ) ) ) {
			return back()->withErrors( [
				'message' => 'Please, check your credentials and try again.'
			] );
		}

		return redirect()->home();

	}

	public function logout()
	{
		auth()->logout();

		return redirect()->home();
	}

}
