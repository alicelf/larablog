@extends('frontend')

@section('headpartials')
	<title>Registration</title>
@endsection

@section('content')

	<div class="blog-main">
		<h3>Registration</h3>
		<hr>

		<form action="/register" method="POST">
			{{csrf_field()}}

			<div class="form-group row">
				<label for="name-id" class="col-3 col-form-label">Name</label>
				<div class="col-9">
					<input type="text" name="name" class="form-control" id="name-id" placeholder="Name">
				</div>
			</div>

			<div class="form-group row">
				<label for="email-id" class="col-3 col-form-label">Email</label>
				<div class="col-9">
					<input type="email" name="email" class="form-control" id="email-id" placeholder="Email">
				</div>
			</div>

			<div class="form-group row">
				<label for="password-id" class="col-3 col-form-label">Password</label>
				<div class="col-9">
					<input type="password" name="password" class="form-control" id="password-id" placeholder="Password">
				</div>
			</div>

			<div class="form-group row">
				<label for="password_confirmation-id" class="col-3 col-form-label">Password Confirmation</label>
				<div class="col-9">
					<input type="password" name="password_confirmation" class="form-control" id="password_confirmation-id" placeholder="Password Confirmation">
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>

		</form>
		@include('common.form_errors')
	</div>

@endsection