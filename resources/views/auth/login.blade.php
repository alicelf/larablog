@extends('frontend')

@section('headpartials')
	<title>Sign In</title>
@endsection

@section('content')

	<div class="blog-main">
		<form action="/signin" method="POST">

			{{csrf_field()}}

			<div class="form-group row">
				<label for="usermail-id" class="col-3 col-form-label">Email</label>
				<div class="col-9">
					<input type="email" name="email" class="form-control" id="usermail-id" placeholder="Email">
				</div>
			</div>

			<div class="form-group row">
				<label for="password-id" class="col-3 col-form-label">Password</label>
				<div class="col-9">
					<input type="password" name="password" class="form-control" id="password-id" placeholder="Password">
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Sign In</button>
			</div>

			<div class="form-group">
				@include('common.form_errors')
			</div>
		</form>
	</div>

@endsection