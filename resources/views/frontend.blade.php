<!doctype html>
<html lang="en-US">
<head>
	{{--Head main data--}}
	@include('front.head')
	{{--There will be dynamic data, such as title, keywords, e.t.c--}}
	@yield('headpartials')
</head>
<body>

	<div id="app">
		@include('front.nav')

		<div class="container">
			@yield('content')
		</div>
	</div>

	{{--Footer Scripts--}}
	@include('front.footer_links')
</body>
</html>