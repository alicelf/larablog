<h4>Archives:</h4>
<div class="card">
	<ul class="list-group list-group-flush">
		@foreach ($archives as $stats)
			<li class="list-group-item">
				<a href="/posts?month={{$stats['month']}}&year={{$stats['year']}}">
					{{$stats['year']}} {{$stats['month']}} ({{$stats['published']}})
				</a>
			</li>
		@endforeach
	</ul>
</div>