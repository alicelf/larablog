@extends('frontend')
@section('headpartials')
	<title>{{$task->title}}</title>
@endsection

@section('content')

	<br>
	<p><a href="/tasks">Back to tasks</a></p>
	{{$task->body}}
	<hr>

@endsection