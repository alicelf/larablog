@extends('frontend')

@section('headpartials')
<title>Tasks Index</title>
@endsection

@section('content')

@foreach($tasks as $task)

	<br>
	<a href="/tasks/{{$task->id}}">{{$task->body}}</a>
	<hr>

@endforeach

@endsection