@extends('frontend')

@section('headpartials')
	<title>Create a post</title>
@endsection

@section('content')

	<h4 class="blog-title">Create a Post</h4>
	<hr>
	<div class="row">
		<div class="col-md-8">
			<form action="/posts" method="POST">
				{{csrf_field()}}
				<div class="form-group">
					<input type="text" name="title"class="form-control">
				</div>
				<div class="form-group">
					<textarea name="body" class="form-control"></textarea>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
		<aside class="col-md-4">
			@include('front.partials.sidebar')
		</aside>
	</div>

	@include('common.form_errors')

@endsection