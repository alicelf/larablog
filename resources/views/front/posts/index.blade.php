@extends('frontend')

@section('headpartials')
	<title>Posts</title>
@endsection

@section('content')

	<div class="blog-main">
		<div class="row">
			<div class="col-md-8">
				@foreach($posts as $post)
					@include('front.posts.loop_single')
				@endforeach
			</div>

			<aside class="col-md-4">
				@include('front.partials.sidebar')
			</aside>
		</div>
	</div>

@endsection