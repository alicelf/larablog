@extends('frontend')

@section('headpartials')
	<title>{{$post->title}}</title>
@endsection

@section('content')
	<section class="blog-main">
		<h2 class="blog-title">{{$post->title}}</h2>
		<hr>
		<div class="row">
			<div class="col-md-8">
				<article class="blog-content">
					<p>{{$post->created_at->diffForHumans()}} <strong>{{$post->author->name}}</strong></p>
					{{$post->body}}
				</article>

				<br>
				<h4>Add a Comment:</h4>
				<div class="card">
					<div class="card-block">
						<form action="/posts/{{$post->id}}/comments" method="POST">
							{{csrf_field()}}
							<div class="form-group">
								<textarea name="body" class="form-control" placeholder="Post your comment here"></textarea>
							</div>
							<button type="submit" class="btn btn-primary">Add a comment</button>
						</form>
					</div>
				</div>
				@include('common.form_errors')

				@if(count($post->comments))
					<br>
					<div class="blog-comments">
						<h4>Comments:</h4>

						<ul class="list-group">
							@foreach($post->comments->sortByDesc('created_at') as $comment)
								<li class="list-group-item">
									<strong>{{$comment->created_at->diffForHumans()}}</strong>: &nbsp;
									{{$comment->body}}
								</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
			<aside class="col-md-4">
				@include('front.partials.sidebar')
			</aside>
		</div>

	</section>
@endsection