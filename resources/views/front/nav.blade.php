<div class="blog-masthead">
	<div class="container">
		<nav class="nav blog-nav">
			<a class="nav-link active" href="/">Home</a>
			<a class="nav-link" href="/posts">Posts</a>
			<a class="nav-link" href="/posts/create">New Post</a>
			<a class="nav-link" href="/register">Register</a>
			@if(auth()->check())
				<a class="nav-link ml-auto" href="#">{{auth()->user()->name}}</a>
				<a class="nav-link ml-auto" href="/logout">Logout</a>
			@else
				<a class="nav-link ml-auto" href="/login">Login</a>
			@endif
		</nav>
	</div>
</div>